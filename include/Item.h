#ifndef ITEM_H
#define ITEM_H

#include <iostream>

#include "taxon.h"

class Item
{
    public:
        Item(const Taxon& order, const Taxon &family, const Taxon &genus, const Taxon &species);
        virtual ~Item();
        Item(const Item& other);
        Item& operator=(const Item& other);
        std::string describe();
    protected:
    private:
        Item();
        std::string order;
        std::string family;
        std::string genus;
        std::string species;
        std::string special_trait;
        std::string binomial_first;
        std::string binomial_second;
        char description[1024];
};

#endif // ITEM_H
