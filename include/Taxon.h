#ifndef TAXON_H
#define TAXON_H

#include <iostream>

class Taxon
{
    public:
        Taxon(const std::string name, Taxon* parent = NULL, std::string binomial_name = std::string(), std::string special_trait = std::string());
        Taxon();
        virtual ~Taxon();
        Taxon(const Taxon& other);
        Taxon& operator=(const Taxon& other);
        void addSibling(Taxon *sibling);
        Taxon *getSibling();
        Taxon *getParent();
        std::string getTrait() const { return special_trait; }
        std::string toString() const { return name; }
        std::string getBinomial()  const { return binomial_name; }
    protected:
    private:
        Taxon* parent;
        Taxon* child;
        Taxon* sibling;
        std::string name;
        std::string binomial_name;
        std::string special_trait;
        int count_of_child;
};

#endif // TAXON_H
