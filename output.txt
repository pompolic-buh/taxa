This is a Gladii Gladius. It belongs to the species Gladius, which belongs to the genus Shortswords, which in turn belongs to the family Swords, which belongs to the order of Sharp weapons. It is short

This is a Spathae Spatha. It belongs to the species Zweihander, which belongs to the genus Longwords, which in turn belongs to the family Swords, which belongs to the order of Sharp weapons. It has a long, undulating blade.

This is a Clavae Ferreae Morningstar. It belongs to the species Morningstar, which belongs to the genus Maces, which in turn belongs to the family Clubs, which belongs to the order of Blunt weapons. It is covered in spikes.

This is a Clavae Ferreae Flanged Mace. It belongs to the species Flanged Mace, which belongs to the genus Maces, which in turn belongs to the family Clubs, which belongs to the order of Blunt weapons. It is covered in iron flanges.

