#include "Item.h"

#include <cstdio>

Item::Item(const Taxon& order, const Taxon &family, const Taxon &genus, const Taxon &species)
{
    this->order = order.toString();
    this->family = family.toString();
    this->genus = genus.toString();
    this->species = species.toString();

    this->special_trait = species.getTrait();
    this->binomial_first = genus.getBinomial();
    this->binomial_second = species.getBinomial();

    sprintf(description, "This is a %s %s. It belongs to the species %s, which belongs to the genus %s, which in turn belongs to the family %s, which belongs to the order of %s. %s",
            this->binomial_first.c_str(), this->binomial_second.c_str(), this->species.c_str(),
             this->genus.c_str(), this->family.c_str(), this->order.c_str(), this->special_trait.c_str());
}

Item::~Item()
{
    //dtor
}

Item::Item(const Item& other)
{
    //copy ctor
}

Item& Item::operator=(const Item& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}

std::string Item::describe()
{
    return std::string(description);
}
