#include "Taxon.h"

#include <iostream>

Taxon::Taxon(const std::string name, Taxon* parent,  std::string binomial_name, std::string special_trait)
{
    count_of_child = 0;
    this->name = name;
    this->binomial_name = binomial_name;
    this->special_trait = special_trait;
    if(parent)
    {
        this->parent = parent;


        //tried to implement a tree structure here to be walked in order to generate items
        //but gave up after some segfaults
        if(0)
        //if(parent->child)
        {
            Taxon *sib = parent->child;
            //std::cerr << "parent: " << parent << " parent->child: " << parent->child << std::endl;
            while(sib->sibling)
            {
                sib = sib->sibling;
            }
            sib->sibling = this;
            parent->count_of_child++;
        }
        else
        {
           // parent->child = this;
           // parent->count_of_child++;
        }
    }
}

Taxon::~Taxon()
{
    //dtor
}

Taxon::Taxon(const Taxon& other)
{
    //copy ctor
}

Taxon& Taxon::operator=(const Taxon& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}
